
Education
==================================================

=====
2013 - 2015: **Master** in Moscow Institute of Physics and Technology
=====
Applied Math and Physics / Telecommunication Systems and Networks
--------
* Thesis: Selection of informative variables in the tasks of system identification
* With: Institute of Radio-engineering and Electronics by the Russian Academy of Science

=====
2009 - 2013: **Bachelor** in Moscow Institute of Physics and Technology
=====
Applied Math and Physics / Automated biotechnical systems
--------
* Thesis: Detection of biological signs in spectral analysis of a radio-electronic scan
* With: Central Science Research Institute of Chemistry and Mechanics



