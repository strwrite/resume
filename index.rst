.. Timur Sultanaev Resume master file, created by
   sphinx-quickstart on Mon Mar 30 06:42:34 2020.

Root
==================================================

Welcome to Timur Sultanaev's Resume!
-------

This is my resume as a code, written in reStructuredText, 
built with Sphinx and deployed to Gitlab Pages via Gitlab CI.

I am a software developer with more than 7        
years of professional programming experience.  
Currently living in Munich and **not** looking 
for job change. I publish this CV mostly for   
fun and *because I can*.       

That's not a final version, let's call it v0.0.1 for now :)

.. toctree::
   :maxdepth: 2
   :caption: Stuff:

   work   
   education
   certification
