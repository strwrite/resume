Work Experience
==================================================

=====
2019 - now: **IoT Developer** in Concept Reply
=====
**Vehicle Telemetry Service**
--------
React, Scala, Akka, Kafka Streams, Openshift, AWS (Fargate, CloudFormation), Docker, Jenkins.

* millions of cars
* stream processing of telemetry data
* focus on observability

**Smart City Analytics Platform**
--------
Golang, RabbitMQ, Kubernetes, Kafka, Helm, AWS (s3, EKS), Docker, Gitlab CI, Sonarqube.

* data is requested from remote API and transformed to uniform format
* data points then injected and buffered in message broker (RabbitMQ)
* then passed to Kafka or s3, where EMR takes it to preprocess and provide for further analysis
* a lot of different data sources: environmental, car traffic, meteorological, video processing and other kinds of data

**Car Traffic Data Access and Visualization**
--------
React, Redux, Java, Spring Boot, RabbitMQ, Kafka, Keycloak, Kubernetes, Helm, AWS (RDS, EKS), Docker, PostgreSQL, Gitlab CI, Maven, Sonarqube.

* live data streamed from Kafka to frontend via Websocket/STOMP, filtered through RabbitMQ
* frontend builds state from live stream
* backend keeps snapshot to transfer in stream on first connect

**LoRaWAN - Streetlighting**
--------
LoRaWAN, Vue.js, Java, Spring Boot, RabbitMQ, Protobuf, AWS (Fargate, EC2), Docker, PostgreSQL, MongoDB, git/Gitlab, Maven.

* work with hardware (Nordic Automation Systems - Luminaire Controller IP54)
* a lot of frontend experience

=====
2016 - 2019: **Cloud Software Developer** in Netcracker Technology
=====

**Database as a Service**
--------
Java, Spring, Spring Boot, Golang, Python, bash, Openshift/Kubernetes, Docker, Postgresql, Mongodb, git/Gitlab, Maven, Jenkins, microservice architecture.

* work in distributed command
* team lead (3-4 developers)
* building microservices with REST API working with different databases (mongodb, postgresql, elasticsearch) on Java/SpringBoot and Golang

**Cloud Platform Automation**
--------
Java, Spring, Spring Boot, Python, bash, Openshift/Kubernetes, Docker, Postgresql, Mongodb, git/Gitlab, Maven, Jenkins, microservice architecture.

* work in distributed command
* development of cloud platform services and devops pipelines (60%)

  * writing libraries to speed up microservice building
    
  * integrate data from various backend microservices
  * writing and running autotests and documentation

**Digital Marketplace**
--------

* operations, incidents, support (40%)

  * monitoring, logs analysis, tuning of infrastructure
  * troubleshooting dev and prod problems across multiple environments
  * coaching other employees - introduction in cloud development

=====
2015 - 2016: **Senior Computer Operator** 
=====

* mandatory military service
* special desktop application: optimization, visualization 
* team lead (3 developers)

=====
2013 - 2015: **Backend Software Developer** in Netcracker Technology
=====
**Telecom Operations and Management Solutions**
--------
Java EE (EJB), JDBC, Spring, Oracle SQL, pl\sql, Weblogic, html, Javascript, Ant, Maven, svn.

* work in distributed command
* support team lead (4-6 developers)
* 2013-2014: autotests(20%), support(20%), documentation(10%), development(50%)
* 2014-2015: support team lead (40%, 4-6 developers), support(10%), development(50%)
* provided trainings about databases to employees and coaching in Netcracker MIPT center