window.addEventListener("load", () => {
    const logos = document.getElementsByClassName("logo")
    if (logos.length > 0) {
        for (let index = 0; index < logos.length; index++) {
            const element = logos.item(index)
            if (element.tagName.toLowerCase() == "img") {
                const img = element
                const initialHeight = img.getBoundingClientRect().height
                document.body.onscroll = (e) => {
                    const diff = Math.min(window.scrollY, 100)
                    const current = parseInt(img.getAttribute("height"))
                    const desired = initialHeight - diff
                    if (!current || current != desired) {
                        img.setAttribute("height", desired.toString())
                    }
                }
                break;
            }
        }

    }
});