Certification
==================================================

=====
Kubernetes
=====

Scored 94% at CNCF CKA 

.. raw:: html

    <div data-iframe-width="400" data-iframe-height="270" data-share-badge-id="34d6ac46-9388-49d2-8ac2-98badf002876" data-share-badge-host="https://www.youracclaim.com"></div><script type="text/javascript" async src="//cdn.youracclaim.com/assets/utilities/embed.js"></script>
